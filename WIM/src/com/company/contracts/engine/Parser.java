package com.company.contracts.engine;

import com. company.contracts.engine.Command;

import java.util.List;

public interface Parser {

    Command parseCommand(String fullCommand);

    List<String> parseParameters(String fullCommand);
}
