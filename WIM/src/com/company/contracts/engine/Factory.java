package com.company.contracts.engine;

import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Bug;
import com.company.contracts.workItems.Comment;
import com.company.contracts.workItems.Feedback;
import com.company.contracts.workItems.Story;
import com.company.model.Team;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;


public interface Factory {


    Team createTeam(String name);

    Board createBoard(String boardName, String teamName);

    Bug createBug(int id, String title, String description, String stepsToReproduce, Priority priority, BugSeverity bugSeverity, Status status, String teamName, String boardName);

    Member createMember(String name);

    Story createStory(int id, String title, String description, Priority priority, StorySize storySize, Status status, String teamName, String boardName);

    Feedback createFeedback(int id, String title, String description, int rating, Status status, String teamName, String boardName);

    Comment createComment(String title, String description);
}