package com.company.contracts.engine;

public interface Reader {
    String readLine();
}
