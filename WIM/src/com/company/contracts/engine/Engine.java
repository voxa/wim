package com.company.contracts.engine;

import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.*;
import com.company.model.Team;

import java.util.Map;

public interface Engine {


    void start();

    Map<String, Team> getTeams();

    Map<String, Member> getMembers();

    Map<String, Board> getBoards();

    Map<Integer, WorkItem> getWorkItems();

    Map<Integer, Feedback> getFeedbacks();

    Map<Integer, Story> getStories();

    Map<Integer, Bug> getBugs();

    Map<Integer, Task> getTasks();

    int getId();
}
