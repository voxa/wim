package com.company.contracts.engine;

import java.util.List;

public interface Command {

    String execute(List<String> parameters);


}
