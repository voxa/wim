package com.company.contracts.engine;

public interface Writer {
    void write(String message);

    void writeLine(String message);
}
