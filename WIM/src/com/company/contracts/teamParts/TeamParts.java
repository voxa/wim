package com.company.contracts.teamParts;

import com.company.contracts.workItems.WorkItem;

import java.util.List;

public interface TeamParts  {


    String getName();

    List<WorkItem> getWorkItem();

    List<String> getActivityHistory();

}
