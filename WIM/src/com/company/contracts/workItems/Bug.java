package com.company.contracts.workItems;


import com.company.contracts.teamParts.Member;
import com.company.model.enums.BugSeverity;

public interface Bug extends Task {




    BugSeverity getBugSeverity();

    Member getAssignee();

    String getStepsToReproduce();

}
