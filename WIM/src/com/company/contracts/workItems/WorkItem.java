package com.company.contracts.workItems;


import com.company.model.enums.Status;

public interface WorkItem {


     int getId();

    String getTitle();

    String getDescription();

    Status getStatus();

    String getWorkItemType();

    String getHistory();

    String getComments();

    void setComments(Comment comments);

    void setHistory(String history);




}
