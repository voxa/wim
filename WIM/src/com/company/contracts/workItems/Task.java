package com.company.contracts.workItems;


import com.company.contracts.teamParts.Member;
import com.company.model.enums.Priority;

public interface Task extends WorkItem {


   Priority getPriority();


   void setAssignee(Member member);


}
