package com.company.contracts.workItems;


import com.company.model.enums.StorySize;

public interface Story extends WorkItem {


    StorySize getStorySize() ;


}
