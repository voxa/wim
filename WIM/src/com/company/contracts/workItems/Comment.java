package com.company.contracts.workItems;


public interface Comment {

    String getAuthor();

    String getComment();

}
