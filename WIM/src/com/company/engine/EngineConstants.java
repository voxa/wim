package com.company.engine;

public class EngineConstants {


    // Success messages

    public static final String PersonCreatedSuccessMessage = "*** Person \"%s\" created ";
    public static final String TeamCreatedSuccessMessage = "*** Team \"%s\" created ";
    public static final String BoardCreatedSuccessMessage = "*** Board \"%s\" created in team %s ";

    public static final String BugCreatedSuccessMessage = "*** Bug Id%d %s created in team \"%s\" and board \"%s\" ";
    public static final String StoryCreatedSuccessMessage = "*** Story Id%d %s created in team \"%s\" and board \"%s\" ";
    public static final String FeedbackCreatedSuccessMessage = "*** Feedback Id%d %s created in team \"%s\" and board \"%s\" ";

    public static final String MemberAddedToTeamSuccessMessage = "*** \"%s\" added to team \"%s\" ";
    public static final String CommentAddedSuccessMessage = "*** Comment with author \"%s\" was added to %s with Id%s";

    public static final String WorkItemAssignedSuccessMessage = "*** \"%s\" was assigned a %s with Id%s  ";
    public static final String MemberAssignedSuccessMessage = "~ was assigned a %s with Id%s  ";
    public static final String MemberUnassignedSuccessMessage = "~ was unassigned a %s with Id%s  ";
    public static final String WorkItemUnassignedSuccessMessage = "*** \"%s\" was unassigned a %s with Id%s ";
    public static final String ChangedTypeSuccessMessage = "*** Type %s of %s changed to : %s ";

    // Error messages

    public static final String PersonExistsErrorMessage = ">>> Name \"%s\" already exists in team or members ";
    public static final String InvalidCommandNameMessage = ">>> Invalid command name: %s!";

    public static final String PersonNoExistsErrorMessage = ">>> Person \"%s\" not found ";
    public static final String PersonInTeamNoExistsErrorMessage = ">>> Person \"%s\" not found in team \"%s\" ";
    public static final String TeamNoExistsErrorMessage = ">>> Team \"%s\" not found ";
    public static final String BoardNoExistsErrorMessage = ">>> Board \"%s\" not found in team \"%s\" ";
    public static final String WorkItemsNoExistsErrorMessage = ">>> WorkItems with Id %d not found ";

    public static final String MemberNoExistsErrorMessage = ">>> Member with name \"%s\" not exists ";
    public static final String MemberExistsErrorMessage = ">>> Member with name \"%s\" already exists in team \"%s\" ";
    public static final String TeamExistsErrorMessage = ">>> Team \"%s\" already exists ";
    public static final String BoardExistsErrorMessage = ">>> Board with name \"%s\" already exists in team \"%s\" ";

    public static final String EnterTeamNameMessage = ">>> Please enter team name ";
    public static final String EnterPersonNameMessage = ">>> Please enter person name ";
    public static final String EnterCorrectMessage = ">>> Please enter correct parameters ";
    public static final String EnterParametersMessage = ">>> Enter correct number of parameters! Must be %d ";
    public static final String InvalidTypeMessage = ">>> %s not found ";
    public static final String EmptyListMessage = ">>> There are no Work Items created";
    public static final String NameLengthError = ">>> Name must be between 5 and 15 symbols";
}