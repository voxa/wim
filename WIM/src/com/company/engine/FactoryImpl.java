package com.company.engine;

import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Bug;
import com.company.contracts.workItems.Comment;
import com.company.contracts.workItems.Feedback;
import com.company.contracts.workItems.Story;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;
import com.company.model.teamParts.BoardImpl;
import com.company.model.teamParts.MemberImpl;
import com.company.model.workItems.BugImpl;
import com.company.model.workItems.CommentImpl;
import com.company.model.workItems.FeedbackImpl;
import com.company.model.workItems.StoryImpl;

public class FactoryImpl implements Factory {


    public FactoryImpl() {
    }

    public Member createMember(String name ) {
        return new MemberImpl(name);
    }

    public Team createTeam(String teamName) {
        return new TeamImpl(teamName);
    }

    public Board createBoard(String boardName, String teamName) {
        return new BoardImpl(boardName);
    }

    public Bug createBug(int id ,String title, String description, String stepsToReproduce ,Priority priority, BugSeverity bugSeverity, Status status, String teamName, String boardName) {
        return new BugImpl(id,title, description, stepsToReproduce ,priority, bugSeverity, status);
    }

    public Story createStory(int id,String title, String description, Priority priority, StorySize storySize, Status status, String teamName, String boardName) {
        return new StoryImpl(id,title, description, priority, storySize, status);
    }

    public Feedback createFeedback(int id, String title, String description, int rating , Status status, String teamName, String boardName) {
        return new FeedbackImpl(id,title, description, rating, status);
    }

    public Comment createComment(String title, String description) {
        return new CommentImpl(title, description);
    }


}
