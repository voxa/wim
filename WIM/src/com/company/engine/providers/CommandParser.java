package com.company.engine.providers;


import com.company.commands.addCommands.AddCommentCommand;
import com.company.commands.addCommands.AddMemberToTeamCommand;
import com.company.commands.changeCommands.ChangeAssigneeCommand;
import com.company.commands.changeCommands.ChangeCommand;
import com.company.commands.createCommands.*;
import com.company.commands.listCommands.*;
import com.company.commands.sortCommands.*;
import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.engine.Parser;
import com.company.engine.EngineConstants;


import java.util.ArrayList;
import java.util.List;



public class CommandParser implements Parser {


    private final Factory factory;
    private final Engine engine;

    public CommandParser(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    public Command parseCommand(String fullCommand) {
        String commandName = fullCommand.split(" ")[0];
        return findCommand(commandName);
    }

    public List<String> parseParameters(String fullCommand) {
        String[] commandParts = fullCommand.split("\\| ");
        ArrayList<String> parameters = new ArrayList<>();
        for (int i = 1; i < commandParts.length; i++) {
            parameters.add(commandParts[i].trim());
        }

        return parameters;
    }

    private Command findCommand(String commandName) {
        switch (commandName) { //switch (commandName.toLowerCase()) {
            case "createPerson":
                return new CreatePersonCommand(factory, engine);

            case "createTeam":
                return new CreateTeamCommand(factory, engine);

            case "createBoard":
                return new CreateBoardCommand(factory, engine);

            case "createBug":
                return new CreateBugCommand(factory, engine);

            case "createStory":
                return new CreateStoryCommand(factory, engine);

            case "createFeedback":
                return new CreateFeedbackCommand(factory, engine);

            case "addMember":
                return new AddMemberToTeamCommand( engine);

            case "showAllPeople":
                return new ListAllPeopleCommand( engine);

            case "showTeams":
                return new ListAllTeamsCommand( engine);

            case "showTeamMembers":
                return new ListAllTeamMembersCommand( engine);

            case "showTeamBoards":
                return new ListAllTeamBoardsCommand( engine);

            case "showWorkItems":
                return new ListAllWorkItemsCommand( engine);

            case "showByType":
                return new ListWorkItemsByTypeCommand( engine);

            case "showByStatus":
                return new ListWorkItemsByStatusCommand(engine);

            case "showByAssignee":
                return new ListWorkItemsByAssigneeCommand( engine);

            case "showByStatusAndAssignee":
                return new ListWorkItemsByStatusAndAssigneeCommand( engine);

            case "showMemberActivity":
                return new ListMemberActivityCommand( engine);

            case "showTeamActivity":
                return new ListTeamActivityCommand(engine);

            case "showBoardActivity":
                return new ListBoardActivityCommand( engine);

            case "change":
                return new ChangeCommand( engine);

            case "addComment":
                return new AddCommentCommand( engine);

            case "changeAssignee":
                return new ChangeAssigneeCommand( engine);

            case "sоrtByTitle":
                return new ListItemsSortedByTitleCommand(engine);

            case "sоrtByRating":
                return new ListItemsSortedByRatingCommand( engine);

            case "sоrtBySize":
                return new ListItemsSortedBySizeCommand( engine);

            case "sоrtByPriority":
                return new ListItemsSortedByPriorityCommand( engine);

            case "sоrtBySeverity":
                return new ListItemsSortedBySeverityCommand( engine);

        }
        throw new IllegalArgumentException(String.format(EngineConstants.InvalidCommandNameMessage, commandName));
    }
}
