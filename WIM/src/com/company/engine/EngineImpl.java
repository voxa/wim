package com.company.engine;

import com.company.contracts.engine.*;
import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.*;
import com.company.engine.providers.CommandParser;
import com.company.engine.providers.ConsoleReader;
import com.company.engine.providers.ConsoleWriter;
import com.company.model.Team;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EngineImpl implements Engine {

    private static final String TERMINATION_COMMAND = "Exit";


    private Reader reader;
    private Writer writer;
    private Parser parser;

    private Map<String, Team> teams;   // >>> MAP
    private Map<String, Member> members;
    private Map<String, Board> boards;
    private Map<Integer, WorkItem> workItems;
    private Map<Integer, Bug> bugs;
    private Map<Integer, Story> stories;
    private Map<Integer, Feedback> feedbacks;
    private Map<Integer, Task> tasks;
    private int id = 1;

    public EngineImpl(Factory factory) {

        reader = new ConsoleReader();
        writer = new ConsoleWriter();
        parser = new CommandParser(factory, this);

        // HASH MAP >>> Key >> Value
        // One object is used as a key (index) to another object (value).
        // It can store different types: String keys and Integer values,
        // or the same type, like: String keys and String values:

        this.teams = new HashMap<>(); //  >>>> key -> value //
        this.members = new HashMap<>();
        this.boards = new HashMap<>();
        this.workItems = new HashMap<>();
        this.bugs = new HashMap<>();
        this.stories = new HashMap<>();
        this.feedbacks = new HashMap<>();
        this.tasks = new HashMap<>();
        this.id = id++;

    }//

    public EngineImpl() {

    }


    public int getId() {
        return id++;
    }

    @Override
    public Map<String, Team> getTeams() {
        return this.teams;
    }

    @Override
    public Map<String, Member> getMembers() {
        return this.members;
    }

    @Override
    public Map<String, Board> getBoards() {
        return this.boards;
    }

    @Override
    public Map<Integer, WorkItem> getWorkItems() {
        return workItems;
    }

    @Override
    public Map<Integer, Task> getTasks() {
        return tasks;
    }

    @Override
    public Map<Integer, Story> getStories() {
        return this.stories;
    }

    @Override
    public Map<Integer, Bug> getBugs() {
        return bugs;
    }

    @Override
    public Map<Integer, Feedback> getFeedbacks() {
        return feedbacks;
    }


    @Override
    public void start() {
        while (true) {
            try {
                String commandAsString = reader.readLine();
                if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
                    break;
                }
                processCommand(commandAsString);
            } catch (Exception ex) {
                writer.writeLine(ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
                //writer.writeLine("####################");
            }
        }
    }

    private void processCommand(String commandAsString) {
        if (commandAsString == null || commandAsString.trim().equals("")) {
            throw new IllegalArgumentException("Command cannot be null or empty.");
        }
        Command command = parser.parseCommand(commandAsString);
        List<String> parameters = parser.parseParameters(commandAsString);
        String executionResult = command.execute(parameters);
        writer.writeLine(executionResult);
    }

}

