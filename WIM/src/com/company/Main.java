package com.company;

import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;

public class Main {

    public static void main(String[] args) {

        Factory factory = new FactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();

    }

}
