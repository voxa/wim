package com.company.model.workItems;

import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Task;
import com.company.model.enums.Priority;


public abstract class TaskImpl extends WorkItemImpl implements Task {


    private Priority priority;
    private Member assignee;

    TaskImpl(int id, String title, String description, Priority priority) {
        super(id, title, description);
        setPriority(priority);
        setAssignee(assignee);
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public void setAssignee(Member assignee) {

        this.assignee = assignee;
    }

    public Member getAssignee() {
        return assignee;
    }

    public Priority getPriority() {
        return priority;
    }

    @Override
    public abstract String getWorkItemType();


    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb. append("       ")
                .append("Priority: ")
                .append(getPriority())
                .append("   ")
                .append("Assignee: ")
                .append(getAssignee() == null ? "No assignee" : getAssignee().getName())
                .append("   ");

        return sb.toString();
    }


}
