package com.company.model.workItems;

import com.company.contracts.workItems.Story;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;


public class StoryImpl extends TaskImpl implements Story {


    private static final String STORY_STATUS_ERROR = ">>> Story status must be Not done , In progress or Done!";
    private static final String STORY_SIZE_ERROR = ">>> Story size must be Large , Medium or Small!";
    private StorySize storySize;


    public StoryImpl(int id, String title, String description, Priority priority, StorySize storySize, Status status) {
        super(id, title, description, priority);

        setStorySize(storySize);
        setStatus(status);
    }

    @Override
    public String getWorkItemType() {
        return "Story";
    }

    public void setStatus(Status status) {

        if (status != Status.NOT_DONE && status != Status.IN_PROGRESS && status != Status.DONE) {
            throw new IllegalArgumentException(STORY_STATUS_ERROR);
        }
        this.status = status;
    }

    public void setStorySize(StorySize storySize) {

        if (storySize != StorySize.LARGE && storySize != StorySize.MEDIUM && storySize != StorySize.SMALL) {
            throw new IllegalArgumentException(STORY_SIZE_ERROR);
        }
        this.storySize = storySize;
    }

    public StorySize getStorySize() {
        return storySize;
    }

    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder(super.getDetails());
        sb.append("Size: ")
                .append(getStorySize())
                .append("   ")
                .append("Status: ")
                .append(getStatus());

        return sb.toString();
    }
}

//
