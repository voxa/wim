package com.company.model.workItems;


import com.company.contracts.workItems.Comment;


public class CommentImpl implements Comment {

    private String author;
    private String comment;


    public CommentImpl(String author, String comment) {
        this.author = author;
        this.comment = comment;
    }

    @Override
    public String getAuthor() {
        return author;
    }


    @Override
    public String getComment() {
        return comment;
    }


    public String toString() {


        StringBuilder sb = new StringBuilder();

        sb.append("       --- ")
                .append("Author: ")
                .append(getAuthor())
                .append("    ")
                .append("Comment: ")
                .append(getComment());

        return sb.toString();
    }
}


