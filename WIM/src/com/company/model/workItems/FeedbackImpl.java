package com.company.model.workItems;

import com.company.contracts.workItems.Feedback;
import com.company.model.enums.Status;

public class FeedbackImpl extends WorkItemImpl implements Feedback {


    private static final String FEEDBACK_STATUS_ERROR = ">>> Feedback status must be New,Scheduled, Unscheduled or Done!";
    private int rating;

    public FeedbackImpl(int id, String title, String description, int rating, Status status) {
        super(id, title, description);

        setRating(rating);
        setStatus(status);
    }

    @Override
    public String getWorkItemType() {
        return "Feedback";
    }


    public void setStatus(Status status) {

        if (status != Status.NEW && status != Status.UNSCHEDULED && status != Status.SCHEDULED && status != Status.DONE) { // PO KRATKO !!!
            throw new IllegalArgumentException(FEEDBACK_STATUS_ERROR);
        }
        this.status = status;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        if (rating < 0) {
            throw new IllegalArgumentException("Rating cannot be less than zero.");
        }
        this.rating = rating;
    }

    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append("       ")
                .append("Rating: ")
                .append(getRating())
                .append("   ")
                .append("Status: ")
                .append(getStatus());

        return sb.toString();
    }

}
