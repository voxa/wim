package com.company.model.workItems;

import com.company.contracts.workItems.Bug;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;

import java.util.Arrays;
import java.util.List;


public class BugImpl extends TaskImpl implements Bug {

    private static final String BUG_SEVERITY_ERROR = ">>> Bug severity must be Critical, Major or Minor!";
    private static final String BUG_STATUS_ERROR = ">>> Bug status must be Active or Fixed!";

    private List<String> stepsToReproduce;
    private BugSeverity bugSeverity;


    public BugImpl(int id, String title, String description, String stepsToReproduce,
                   Priority priority, BugSeverity bugSeverity, Status status) {
        super(id, title, description, priority);

        this.stepsToReproduce = this.stepConvert(stepsToReproduce);
        setBugSeverity(bugSeverity);
        setStatus(status);
    }


    @Override
    public void setStatus(Status status) {

        if (status != Status.ACTIVE && status != Status.FIXED) {
            throw new IllegalArgumentException(BUG_STATUS_ERROR);
        }
        this.status = status;
    }


    @Override
    public String getWorkItemType() {
        return "Bug";
    }

    public void setBugSeverity(BugSeverity bugSeverity) {

        if (bugSeverity != BugSeverity.CRITICAL && bugSeverity != BugSeverity.MAJOR && bugSeverity != BugSeverity.MINOR) { // ? po-kratnko
            throw new IllegalArgumentException(BUG_SEVERITY_ERROR);
        }
        this.bugSeverity = bugSeverity;
    }


    public BugSeverity getBugSeverity() {
        return bugSeverity;
    }


    @Override
    public String getDetails() {
        StringBuilder sb = new StringBuilder(super.getDetails());
        sb.append("Severity: ")
                .append(getBugSeverity())
                .append("   ")
                .append("Status: ")
                .append(getStatus())
                .append("\n")
                .append("       ")
                .append("Steps to reproduce: ")
                .append(getStepsToReproduce().isEmpty() ? "No steps" : getStepsToReproduce());

        return sb.toString();
    }


    public String getStepsToReproduce() {
        return String.join(" --> ", this.stepsToReproduce);
    }

    private List<String> stepConvert(String stepsToReproduce) {
        if (stepsToReproduce != null && !stepsToReproduce.isEmpty()) {
            String[] step = stepsToReproduce.split(" >> ");
            return this.stepsToReproduce = Arrays.asList(step);
        } else {
            throw new IllegalArgumentException(" You must add steps, separated by ' >> '");
        }
    }
}

