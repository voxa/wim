package com.company.model.workItems;


import com.company.contracts.workItems.Comment;
import com.company.contracts.workItems.WorkItem;
import com.company.model.enums.Status;

import java.util.ArrayList;
import java.util.List;

public abstract class WorkItemImpl implements WorkItem {

    private static final String TITLE_ERROR_MESSAGE = "Title cannot be null or empty!";
    private static final String DESCRIPTION_ERROR_MESSAGE = "Description cannot be null or empty!";
    private static final int TITLE_MIN = 10;
    private static final int TITLE_MAX = 50;
    private static final int DESCRIPTION_MIN = 10;
    private static final int DESCRIPTION_MAX = 500;
    private static final String TITLE_ERROR_LENGTH = "Title must be between %d and %d symbols!";
    private static final String DESCRIPTION_ERROR_LENGTH = "Description must be between %d and %d symbols!";
    public int id;
    private String title;
    private String description;
    public Status status;
    private List<Comment> comments;
    private List<String> histories;


   public WorkItemImpl(int id, String title, String description) {

        setTitle(title);
        setDescription(description);
          comments = new ArrayList<>();
        histories = new ArrayList<>();
        setId(id);

    }


    private void setDescription(String description) {

        if (description == null || description.isEmpty()) {

            throw new IllegalArgumentException(DESCRIPTION_ERROR_MESSAGE);
        }
        if (description.length() < DESCRIPTION_MIN || description.length() > DESCRIPTION_MAX) {

            throw new IllegalArgumentException(String.format(DESCRIPTION_ERROR_LENGTH, DESCRIPTION_MIN, DESCRIPTION_MAX));
        }

        this.description = description;
    }

    private void setTitle(String title) {

        if (title == null || title.isEmpty()) {

            throw new IllegalArgumentException(TITLE_ERROR_MESSAGE);
        }
        if (title.length() < TITLE_MIN || title.length() > TITLE_MAX) {

            throw new IllegalArgumentException(String.format(TITLE_ERROR_LENGTH, TITLE_MIN, TITLE_MAX));
        }
        this.title = title;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public abstract String getWorkItemType();


    public String getTitle() {
        return title;
    }


    public String getDescription() {
        return description;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setComments(Comment comment) {

        this.comments.add(comment);
    }

    public String getComments() {

        return comments
                .toString()
                .replace("[","")
                .replace("]","");
    }

    public void setHistory(String history) {
        this.histories.add(history);
    }

    public String getHistory() {

        return histories
                .toString()
                .replace("[","")
                .replace("]","");
                    }

    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("Id").append(getId())
                .append(" ").append(getWorkItemType())
                .append("\n")
                .append("       ")
                .append("Title: ")
                .append(getTitle())
                .append("   ")
                .append("Description: ")
                .append(getDescription())
                .append("\n")
                       .append(getDetails())
                .append("\n")
                .append("       ")
                .append("Comments: ")
                 .append(getComments().isEmpty() ? "No comments" : "\n"+getComments())
                .append("\n")
                .append("       ")
                .append("History: ")
                .append(getHistory().isEmpty() ? "No history" : "\n"+getHistory()) ;

        return sb.toString();
    }

    protected abstract String getDetails();
}
