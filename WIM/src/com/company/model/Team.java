package com.company.model;

import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;


import java.util.List;
import java.util.Map;

public interface Team {


    String getTeamName() ;

    Map<String, Board> getBoards();

    Map<String, Member> getMembers();

    List<String> getTeamActivity();



    }
