
package com.company.model;

import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeamImpl implements Team {


    private static final String TEAM_NAME_ERROR = "Team %s cannot be null or empty";
    private String teamName;

    private Map<String, Member> members;
    private Map<String, Board> boards;
    private List<String> teamActivity;


    public TeamImpl(String teamName) {
        setTeamName(teamName);
        this.members = new HashMap();
        this.boards = new HashMap();
        setTeamActivity(new ArrayList<>());
    }


    public List<String> getTeamActivity() {
        return teamActivity;
    }

    private void setTeamActivity(List<String> teamActivity) {
        this.teamActivity = new ArrayList<>(teamActivity);
    }

    public String getTeamName() {
        return teamName;
    }

    private void setTeamName(String teamName) {
        if (teamName == null || teamName.isEmpty()) {
            throw new IllegalArgumentException(String.format(TEAM_NAME_ERROR, teamName));
        }
        this.teamName = teamName;
    }


    public Map<String, Member> getMembers() {
        return members;
    }


    public Map<String, Board> getBoards() {
        return boards;
    }
}
