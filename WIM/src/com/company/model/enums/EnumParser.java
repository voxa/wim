package com.company.model.enums;

import com.company.contracts.workItems.Bug;

import javax.print.attribute.standard.Severity;

public class EnumParser {

    public static Priority parsePriority(String value) {
        Priority result;
        try {
            result = Priority.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(">>> \"%s\" - invalid Priority value!", value));
        }
        return result;
    }

    public static BugSeverity parseSeverity(String value) {
        BugSeverity result;
        try {
            result = BugSeverity.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(">>> \"%s\" - invalid Severity value!", value));
        }
        return result;
    }

    public static Status parseStatus(String value) {
        Status result;
        try {
            result = Status.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(">>> \"%s\" - invalid Status value!", value));
        }
        return result;
    }


    public static StorySize parseStorySize(String value) {
        StorySize result;
        try {
            result = StorySize.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(String.format(">>> \"%s\" - invalid Story Size value!", value));
        }
        return result;
    }
}
