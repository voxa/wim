package com.company.model.enums;

public enum Status {



    // -----------  BUG STATUS
    ACTIVE,
    FIXED,
    // ----------- STORY STATUS
    NOT_DONE,
    IN_PROGRESS,
    DONE,
    // ----------- FEEDBACK STATUS
    NEW,
    UNSCHEDULED,
    SCHEDULED;
    // DONE;

    public String toString() {
        switch(this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            case NOT_DONE:
                return "Not done";
            case IN_PROGRESS:
                return "In progress";
            case DONE:
                return "Done";
            case NEW:
                return "New";
            case UNSCHEDULED:
                return "Unscheduled";
            case SCHEDULED:
                return "Scheduled";

            default:
                throw new IllegalArgumentException("Invalid priority type!");
        }
    }




}
