package com.company.model.enums;

public enum StorySize {



    LARGE,
    MEDIUM,
    SMALL;


    public String toString() {
        switch(this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new IllegalArgumentException("The size of the story is invalid!");
        }
    }
}
