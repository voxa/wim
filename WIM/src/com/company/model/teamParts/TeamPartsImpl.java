package com.company.model.teamParts;

import com.company.contracts.teamParts.TeamParts;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.EngineConstants;

import java.util.ArrayList;
import java.util.List;

public abstract class TeamPartsImpl implements TeamParts {

    private static final String NAME_ERROR = ">>> Name cannot be null or empty";
    private static final int NAME_MIN = 5;
    private static final int NAME_MAX = 15;
    private String name;
    private List<WorkItem> workItem;
    private List<String> activityHistory;


    public TeamPartsImpl(String name) {
        setName(name);
        setWorkItem(new ArrayList<>());
        setActivityHistory(new ArrayList<>());

    }


    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException(NAME_ERROR);
        }
        if (name.length() < NAME_MIN || name.length() > getNameMax()) {
            throw new IllegalArgumentException(getNameLengthIllegalArgumentMessage());
        }
        this.name = name;
    }

    protected String getNameLengthIllegalArgumentMessage() {
        return EngineConstants.NameLengthError;
    }

    protected int getNameMax() {
        return NAME_MAX;
    }

    public List<WorkItem> getWorkItem() {
        return workItem;
    }

    private void setWorkItem(List<WorkItem> workItem) {
        this.workItem = new ArrayList<>(workItem);
    }

    public List<String> getActivityHistory() {
        return activityHistory;
    }

    private void setActivityHistory(List<String> activityHistory) {
        this.activityHistory = new ArrayList<>(activityHistory);
    }


//
//    public void addActivityHistory(String str) {
//                  this.activityHistory.add(str);
//         }


}
