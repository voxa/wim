package com.company.model.teamParts;

import com.company.contracts.teamParts.Board;
import com.company.model.enums.Status;


public class BoardImpl extends TeamPartsImpl implements Board {


    private static final String NAME_LENGTH_ERROR = "Board name must be between 5 and 10 symbols";
    private static final int NAME_MAX = 10;

    public BoardImpl(String name) {
        super(name);
    }

    public  int getNameMax() {
        return NAME_MAX;
    }

    public String getNameLengthIllegalArgumentMessage() {
        return NAME_LENGTH_ERROR;
    }

}
