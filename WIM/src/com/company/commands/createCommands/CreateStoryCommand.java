package com.company.commands.createCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.workItems.Story;
import com.company.contracts.workItems.Task;
import com.company.engine.EngineConstants;
import com.company.model.Team;
import com.company.model.enums.EnumParser;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;


import java.util.List;

public class CreateStoryCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateStoryCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String title;
        String description;
        Priority priority;
        StorySize storySize;
        Status status;
        //////////
        String teamName;
        String boardName;


        if (parameters.size() != 7) {
            return String.format(EngineConstants.EnterParametersMessage, 7);
        }

            title = parameters.get(0);
            description = parameters.get(1);
            priority = EnumParser.parsePriority(parameters.get(2));
            storySize = EnumParser.parseStorySize(parameters.get(3));
            status = EnumParser.parseStatus(parameters.get(4));
            teamName = parameters.get(5);
            boardName = parameters.get(6);


        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (!this.engine.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.BoardNoExistsErrorMessage, boardName, teamName));
        }

        int key = engine.getId();

        Team tempTeam = engine.getTeams().get(teamName);

        Story story = factory.createStory(key, title, description, priority, storySize, status, teamName, boardName);

        engine.getWorkItems().put(key, story);
        engine.getStories().put(key, story);
        engine.getTasks().put(key, (Task) story); // ???

        tempTeam.getBoards().get(boardName).getWorkItem().add(story);
        tempTeam.getBoards().get(boardName).getActivityHistory().add(String.format("~ Story with Id%d %s was added", key, title));
        tempTeam.getTeamActivity().add(String.format("~ Story with Id%d %s was added", key, title));

        return String.format(EngineConstants.StoryCreatedSuccessMessage, key, title, teamName, boardName);
    }

}