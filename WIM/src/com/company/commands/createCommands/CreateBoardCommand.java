package com.company.commands.createCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Board;
import com.company.engine.EngineConstants;
import com.company.model.Team;

import java.util.List;


public class CreateBoardCommand implements Command {

    private final Factory factory;
    private final Engine engine;

    public CreateBoardCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        String boardName;
        String teamName;

        if (parameters.size() != 2) {
            return String.format(EngineConstants.EnterParametersMessage, 2);
        }

        boardName = parameters.get(0);
        teamName = parameters.get(1);

        if (!engine.getTeams().containsKey(teamName)) {

            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (this.engine.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.BoardExistsErrorMessage, boardName, teamName));
        }

        Team tempTeam = engine.getTeams().get(teamName);
        Board board = factory.createBoard(boardName, teamName);

        engine.getBoards().put(boardName, board);

        tempTeam.getBoards().put(boardName, board);
        tempTeam.getTeamActivity().add(String.format("~ Board \"%s\" was added", boardName));
        engine.getTeams().get(teamName).getBoards().get(boardName)
                .getActivityHistory().add(String.format("~ Board \"%s\" was created", boardName));

        return String.format(EngineConstants.BoardCreatedSuccessMessage, boardName, teamName);
    }

}
