package com.company.commands.createCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.workItems.Feedback;
import com.company.engine.EngineConstants;
import com.company.model.Team;
import com.company.model.enums.EnumParser;
import com.company.model.enums.Status;


import java.util.List;

public class CreateFeedbackCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateFeedbackCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String title;
        String description;
        int rating;
        Status status;
        String teamName;
        String boardName;

        if (parameters.size() != 6) {
            return String.format(EngineConstants.EnterParametersMessage, 6);
        }

        title = parameters.get(0);
        description = parameters.get(1);
        try {
            rating = Integer.parseInt(parameters.get(2));

        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters in feedback creation");
        }

        status = EnumParser.parseStatus(parameters.get(3));
        teamName = parameters.get(4);
        boardName = parameters.get(5);

        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (!this.engine.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.BoardNoExistsErrorMessage, boardName, teamName));
        }

        int key = engine.getId();

        Team tempTeam = engine.getTeams().get(teamName);

        Feedback feedback = factory.createFeedback(key, title, description, rating, status, teamName, boardName);

        engine.getWorkItems().put(key, feedback);
        engine.getFeedbacks().put(key, feedback);

        tempTeam.getBoards().get(boardName).getWorkItem().add(feedback);
        tempTeam.getBoards().get(boardName).getActivityHistory().add(String.format("~ Feedback with Id%d %s was added", key, title));
        tempTeam.getTeamActivity().add(String.format("~ Feedback Id%d %s was added", key, title));

        return String.format(EngineConstants.FeedbackCreatedSuccessMessage, key, title, teamName, boardName);
    }

}