package com.company.commands.createCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineConstants;
import com.company.model.Team;

import java.util.List;

public class CreateTeamCommand implements Command {
    private final Factory factory;
    private final Engine engine;


    public CreateTeamCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String teamName;

        if (parameters.size() == 0) {
            return EngineConstants.EnterTeamNameMessage;
        }
            teamName = parameters.get(0);


        if (engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamExistsErrorMessage, teamName));
        }

        Team team = factory.createTeam(teamName);
        engine.getTeams().put(teamName, team);
        team.getTeamActivity().add(String.format("~ Team \"%s\" was created", teamName));


        return String.format(EngineConstants.TeamCreatedSuccessMessage, teamName);
    }
}