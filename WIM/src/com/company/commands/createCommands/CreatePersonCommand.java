package com.company.commands.createCommands;

import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Member;
import com.company.contracts.engine.Command;
import com.company.engine.EngineConstants;


import java.util.List;

public class CreatePersonCommand implements Command {

    private final Factory factory;
    private final Engine engine;

    public CreatePersonCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String name;


        if (parameters.size() != 1) {
            return EngineConstants.EnterPersonNameMessage;
        }

                 name = parameters.get(0);

        if (engine.getMembers().containsKey(name)) {
            throw new IllegalArgumentException(String.format(EngineConstants.PersonExistsErrorMessage, name));
        }

        Member member = factory.createMember(name);

        engine.getMembers().put(name, member);

        member.getActivityHistory().add(String.format(" %s was created", name));

        return String.format(EngineConstants.PersonCreatedSuccessMessage, name);
    }
}
