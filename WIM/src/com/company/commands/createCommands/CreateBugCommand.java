package com.company.commands.createCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.workItems.Bug;

import com.company.engine.EngineConstants;
import com.company.model.Team;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.EnumParser;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;


import java.util.List;

public class CreateBugCommand implements Command {
    private final Factory factory;
    private final Engine engine;

    public CreateBugCommand(Factory factory, Engine engine) {
        this.factory = factory;
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        String title;
        String description;
        String stepsToReproduce;
        Priority priority;
        BugSeverity bugSeverity;
        Status status;
        String teamName;
        String boardName;

        if (parameters.size() != 8) {
            return String.format(EngineConstants.EnterParametersMessage, 8);
        }

            title = parameters.get(0);
            description = parameters.get(1);
            stepsToReproduce = parameters.get(2);
            priority = EnumParser.parsePriority(parameters.get(3));
            bugSeverity = EnumParser.parseSeverity(parameters.get(4));
            status = EnumParser.parseStatus(parameters.get(5));
            teamName = parameters.get(6);
            boardName = parameters.get(7);



        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (!this.engine.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.BoardNoExistsErrorMessage, boardName, teamName));
        }

        int key = engine.getId();

        Team tempTeam = engine.getTeams().get(teamName);
        Bug bug = factory.createBug(key, title, description, stepsToReproduce, priority, bugSeverity, status, teamName, boardName);

        engine.getBugs().put(key, bug);

        engine.getTasks().put(key, bug);
        engine.getWorkItems().put(key, bug);
        tempTeam.getBoards().get(boardName).getWorkItem().add(bug);
        tempTeam.getBoards().get(boardName).getActivityHistory().add(String.format("~ Bug with Id%d %s was added", key, title));

        return String.format(EngineConstants.BugCreatedSuccessMessage, key, title, teamName, boardName);
    }

}