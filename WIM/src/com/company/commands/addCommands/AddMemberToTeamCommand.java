package com.company.commands.addCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.teamParts.Member;
import com.company.engine.EngineConstants;
import com.company.model.Team;

import java.util.List;

public class AddMemberToTeamCommand implements Command {


    private final Engine engine;

    public AddMemberToTeamCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String name;
        String teamName;

        if (parameters.size() != 2) {
            return String.format(EngineConstants.EnterParametersMessage, 2);
        }

            name = parameters.get(0);
            teamName = parameters.get(1);


        if (!this.engine.getMembers().containsKey(name)) {
            throw new IllegalArgumentException(String.format(EngineConstants.PersonInTeamNoExistsErrorMessage, name , teamName));
        }


        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (this.engine.getTeams().get(teamName).getMembers().containsKey(name)) {
            throw new IllegalArgumentException(String.format(EngineConstants.MemberExistsErrorMessage, name, teamName));
        }


        Team tempTeam = engine.getTeams().get(teamName);
        Member member = engine.getMembers().get(name);

        tempTeam.getMembers().put(name, member);

        tempTeam.getTeamActivity().add(String.format("~ \"%s\" is added to team", name));
        member.getActivityHistory().add(String.format("~ was added to team \"%s\"", teamName));

        return String.format(EngineConstants.MemberAddedToTeamSuccessMessage, name, teamName);
    }

}
