package com.company.commands.addCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.Comment;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.EngineConstants;
import com.company.model.workItems.CommentImpl;


import java.util.List;

public class AddCommentCommand implements Command {

    private final Engine engine;

    public AddCommentCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        int id;
        String author;
        String text;


        if (parameters.size() != 3) {
            return String.format(EngineConstants.EnterParametersMessage, 3);
        }

        try {
            id = Integer.parseInt(parameters.get(0));
            author = parameters.get(1);
            text = parameters.get(2);
        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters");
        }



        WorkItem item = engine.getWorkItems().get(id);

        if (item == null) {
            throw new IllegalArgumentException(String.format(EngineConstants.WorkItemsNoExistsErrorMessage, id));
        }

        if (!this.engine.getMembers().containsKey(author)) {
            throw new IllegalArgumentException(String.format(EngineConstants.PersonNoExistsErrorMessage, author));
        }

        String type = item.getWorkItemType();

        Comment currentComment = new CommentImpl(author, text);

        engine.getWorkItems().get(id).setComments(currentComment);
        engine.getWorkItems().get(id).setHistory(String.format("      --- \"%s\"  added comment ", author));

        engine.getMembers().get(author).getActivityHistory().add(String.format("~ added comment to %s  with Id%s", type, id));

        return String.format(EngineConstants.CommentAddedSuccessMessage, author, type, id);

    }
}


