package com.company.commands.changeCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.EngineConstants;
import com.company.model.Team;

import java.util.List;


public class ChangeAssigneeCommand implements Command {

    private final Engine engine;

    public ChangeAssigneeCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        int id;
        String assigneeName;
        String actionVerb;

        if (parameters.size() != 3) {
            return String.format(EngineConstants.EnterParametersMessage, 3);
        }

        try {
            id = Integer.parseInt(parameters.get(0));
            assigneeName = parameters.get(1);
            actionVerb = parameters.get(2);
        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters");
        }

        WorkItem item = engine.getWorkItems().get(id);

        if (item == null) {
            throw new IllegalArgumentException(String.format(EngineConstants.WorkItemsNoExistsErrorMessage, id));
        }

        if (!engine.getMembers().containsKey(assigneeName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.PersonNoExistsErrorMessage, assigneeName));
        }

        String type = item.getWorkItemType();
        Member member = engine.getMembers().get(assigneeName);

        Team teamOfItem =null ;

        for (Team t : engine.getTeams().values()) { // prowerka che w tozi team ima board s takuw item
            for (Board b : t.getBoards().values()) {
                for (WorkItem w : b.getWorkItem()) {
                    if (w.getId() == id) {
                        teamOfItem = t;
                    }
                }
            }
        }

           if(!teamOfItem.getMembers().containsKey(assigneeName)){
            throw new IllegalArgumentException(String.format("Person \"%s\" is not part of item's team", assigneeName));
        }
             if (!engine.getTasks().containsKey(id)) {
            throw new IllegalArgumentException(String.format("Work item with Id%d %s has not Assignee parameter", id, type));
        }


        if (actionVerb.equals("assign")) {

            if (member.getWorkItem().contains(item)) {
                throw new IllegalArgumentException(String.format("\"%s\" already assigned for work item with Id%d %s", assigneeName, id, type));
            } else {
                member.getWorkItem().add(item);
                engine.getTasks().get(id).setAssignee(member);
                engine.getWorkItems().get(id).setHistory(String.format("Work item with Id%d assigned with \"%s\"", id, assigneeName));
                member.getActivityHistory().add(String.format(EngineConstants.MemberAssignedSuccessMessage, type, id));
            }

            return String.format(EngineConstants.WorkItemAssignedSuccessMessage, assigneeName, type, id);


        } else if (actionVerb.equals("unassign")) {

            if (!member.getWorkItem().contains(item)) {
                throw new IllegalArgumentException(String.format("%s is not assigned for  work item with Id%d %s", assigneeName, id, type));
            } else {
                member.getWorkItem().remove(item);
                member.getActivityHistory().add(String.format(EngineConstants.MemberUnassignedSuccessMessage, type, id));
                engine.getWorkItems().get(id).setHistory(String.format("Work item with Id%d unassigned ", id));
            }

            return String.format(EngineConstants.WorkItemUnassignedSuccessMessage, assigneeName, type, id);

        } else {
            return EngineConstants.EnterCorrectMessage;
        }
    }
}


