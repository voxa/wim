package com.company.commands.changeCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.*;
import com.company.engine.EngineConstants;
import com.company.model.enums.*;
import com.company.model.workItems.*;

import java.util.List;

import static java.lang.String.valueOf;


public class ChangeCommand implements Command {


    private final Engine engine;

    public ChangeCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        int id;
        String typeStatus; // Priority , Severity ...
        String newValue;

        if (parameters.size() != 3) {
            return String.format(EngineConstants.EnterParametersMessage, 3);
        }

              try {
            id = Integer.parseInt(parameters.get(0));
            typeStatus = parameters.get(1);
            newValue = parameters.get(2);
        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters");
        }


        if (!engine.getWorkItems().containsKey( id)) {
            throw new IllegalArgumentException(String.format(EngineConstants.WorkItemsNoExistsErrorMessage, id));
        }



        WorkItem item = engine.getWorkItems().get(id);
      //  Bug tempBug = (Bug) engine.getWorkItems().get(id);

        String print = "";

        if (item instanceof Bug) {

          print = "bug";
            switch (typeStatus) {
                case "priority":
                    ((BugImpl) item).setPriority(EnumParser.parsePriority(newValue));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- priority changed to %s  ", newValue));
                    break;
                case "severity":
                    ((BugImpl) item).setBugSeverity(EnumParser.parseSeverity(newValue));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- severity changed to %s  ", newValue));
                    break;
                case "status":
                    ((BugImpl) item).setStatus(EnumParser.parseStatus(valueOf(newValue)));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- status changed to %s  ", newValue));
                    break;
                default:
                    return String.format(EngineConstants.InvalidTypeMessage, typeStatus);
            }

        } else if (item instanceof Story) {

            print = "story";
            switch (typeStatus) {
                case "priority":
                    ((StoryImpl) item).setPriority(EnumParser.parsePriority(valueOf(newValue)));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- priority changed to %s  ", newValue));
                    break;
                case "size":
                    ((StoryImpl)  item).setStorySize(EnumParser.parseStorySize(valueOf(newValue)));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- size changed to %s  ", newValue));
                    break;
                case "status":
                    ((StoryImpl)  item).setStatus(EnumParser.parseStatus(valueOf(newValue)));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- status changed to %s  ", newValue));
                    break;
                default:
                    return String.format(EngineConstants.InvalidTypeMessage, typeStatus);
            }

        } else if (item instanceof Feedback) {

            print = "feedback";
            switch (typeStatus) {
                case "rating":
                    ((FeedbackImpl) item).setRating(Integer.parseInt(newValue));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- rating changed to %s  ", newValue));
                    break;
                case "status":
                    ((FeedbackImpl) item).setStatus(EnumParser.parseStatus(valueOf(newValue)));
                    engine.getWorkItems().get(id).setHistory(String.format("      --- status changed to %s  ", newValue));
                    break;
                default:
                    return String.format(EngineConstants.InvalidTypeMessage, typeStatus);
            }

        }

        return String.format(EngineConstants.ChangedTypeSuccessMessage, typeStatus, print, newValue);
    }
}


