package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;


public class ListWorkItemsByAssigneeCommand implements Command {
    private final Engine engine;

    public ListWorkItemsByAssigneeCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String assigneeName;

        if (parameters.size() != 1) {
            return ">>> Enter parameter correct ";
        }

        assigneeName = (parameters.get(0));

        String result = "";

        result = engine.getMembers().get(assigneeName).getWorkItem()
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");

        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of workItems assign to %s " +
                        "\n %s" +
                        "\n~~~", assigneeName, result);
    }
}
