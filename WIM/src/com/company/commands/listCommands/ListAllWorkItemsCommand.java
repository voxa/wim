package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;


public class ListAllWorkItemsCommand implements Command {

    private final Engine engine;

    public ListAllWorkItemsCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getWorkItems().values().isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        String result = engine.getWorkItems().values()
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        if (result.isEmpty()) {
            return EngineConstants.EmptyListMessage;
        }

        return String.format(
                "~~~ List of all workItems " +
                        "\n %s" +
                        "\n~~~", result);
    }

}


