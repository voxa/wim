package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;
import com.company.model.enums.EnumParser;
import com.company.model.enums.Status;

import java.util.List;
import java.util.stream.Collectors;


public class ListWorkItemsByStatusCommand implements Command {
    private final Engine engine;

    public ListWorkItemsByStatusCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        Status status;

        if (parameters.size() != 1) {
            return ">>> Enter parameter correct ";
        }

        try {
            status = EnumParser.parseStatus(parameters.get(0));
        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters");
        }

        String result = engine.getWorkItems()
                .keySet()
                .stream()
                .filter(key -> (engine.getWorkItems().get(key).getStatus()).equals(status))
                .map(key -> engine.getWorkItems().get(key))
                .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of workItems by status %s " +
                        "\n %s" +
                        "\n~~~", status, result);
    }
}
