package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;

public class ListBoardActivityCommand implements Command {


    private final Engine engine;

    public ListBoardActivityCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String boardName;
        String teamName;

        if (parameters.size() != 2) {
            return String.format(EngineConstants.EnterParametersMessage, 2);
        }

        boardName = parameters.get(0);
        teamName = parameters.get(1);

        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        if (!engine.getTeams().get(teamName).getBoards().containsKey(boardName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.BoardNoExistsErrorMessage, boardName, teamName));
        }

        String result = engine.getTeams().get(teamName).getBoards().get(boardName)
                .getActivityHistory()
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");

        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List activity of board %s from team %s " +
                        "\n %s" +
                        "\n~~~", boardName, teamName, result);
    }

}