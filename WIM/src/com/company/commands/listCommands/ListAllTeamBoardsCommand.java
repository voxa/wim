package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;

public class ListAllTeamBoardsCommand implements Command {


    private final Engine engine;

    public ListAllTeamBoardsCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String teamName;

        if (parameters.size() != 1) {
            return String.format(EngineConstants.EnterParametersMessage, 1);
        }

        teamName = parameters.get(0);

        if (!engine.getTeams().containsKey(teamName)) {
            throw new IllegalArgumentException(String.format(EngineConstants.TeamNoExistsErrorMessage, teamName));
        }

        String result = engine.getTeams().get(teamName)
                .getBoards()
                .keySet()
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of team %s boards  " +
                        "\n %s" +
                        "\n~~~", teamName, result);
    }

}