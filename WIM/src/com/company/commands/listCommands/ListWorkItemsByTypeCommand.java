package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;


public class ListWorkItemsByTypeCommand implements Command {
    private final Engine engine;

    public ListWorkItemsByTypeCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String type;

        if (parameters.size() != 1) {
            return ">>> Enter parameter correct ";
        }

                  type = parameters.get(0);


        if (!type.equals("Bug") && !type.equals("Story") && !type.equals("Feedback")) {
            return "Enter correct workItem Type ";
        }

        String result = "";

        if (type.equals("Bug")) {

            result = engine.getBugs().values()
                    .toString()
                    .replace(",", "\n")
                    .replace("[", "")
                    .replace("]", "");

        } else if (type.equals("Story")) {

            result = engine.getStories().values()
                    .toString()
                    .replace(",", "\n")
                    .replace("[", "")
                    .replace("]", "");

        } else if (type.equals("Feedback")) {

            result = engine.getFeedbacks().values()
                    .toString()
                    .replace(",", "\n")
                    .replace("[", "")
                    .replace("]", "");
        }


        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of workItems of type %s " +
                        "\n %s" +
                        "\n~~~", type, result);
    }
}
