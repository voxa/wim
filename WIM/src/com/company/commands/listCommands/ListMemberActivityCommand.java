package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;

public class ListMemberActivityCommand implements Command {


    private final Engine engine;

    public ListMemberActivityCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {
        String name;

        if (parameters.size() != 1) {
            return ">>> Enter name ";
        }
        name = parameters.get(0);

        if (!engine.getMembers().containsKey(name)) {
            throw new IllegalArgumentException(String.format(EngineConstants.PersonNoExistsErrorMessage, name));
        }

        String result = engine.getMembers().get(name).getActivityHistory()
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");

        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List activity of person %s " +
                        "\n %s" +
                        "\n~~~", name, result);
    }

}