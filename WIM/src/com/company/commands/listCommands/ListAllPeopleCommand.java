package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.engine.EngineConstants;

import java.util.List;


public class ListAllPeopleCommand implements Command {


    private final Engine engine;

    public ListAllPeopleCommand(Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        String result = engine.getMembers().keySet().toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");

        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of all people  " +
                        "\n %s" +
                        "\n~~~", result);
    }

}