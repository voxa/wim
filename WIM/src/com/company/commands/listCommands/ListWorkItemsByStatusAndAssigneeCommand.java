package com.company.commands.listCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.teamParts.Member;
import com.company.engine.EngineConstants;
import com.company.model.enums.Status;

import java.util.List;
import java.util.stream.Collectors;


public class ListWorkItemsByStatusAndAssigneeCommand implements Command {

    private final Engine engine;

    public ListWorkItemsByStatusAndAssigneeCommand(Engine engine) {
        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {

        Status status;
        String assigneeName;

        if (parameters.size() != 2) {
            return ">>> Enter parameters correct ";
        }

        try {
                status = Status.valueOf(parameters.get(0).toUpperCase());
            assigneeName = (parameters.get(1)); status = Status.valueOf(parameters.get(0).toUpperCase());
        } catch (Exception ex) {
            throw new IllegalArgumentException(" >>> Failed to parse parameters");
        }

        if(!engine.getMembers().containsKey(assigneeName)){

            throw new IllegalArgumentException(String.format(EngineConstants.MemberNoExistsErrorMessage,assigneeName));
        }

        Member member = engine.getMembers().get(assigneeName);

        Status finalStatus = status;

        String result = engine.getWorkItems()
                .keySet()
                .stream()
                .filter(key -> engine.getWorkItems().get(key).getStatus().equals(finalStatus))
                .filter(key -> member.getWorkItem().contains(engine.getWorkItems().get(key)))
                .map(key ->  engine.getWorkItems().get(key))
                .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");

        if (result.isEmpty()) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        return String.format(
                "~~~ List of workItems assign to %s with status %s" +
                        "\n %s" +
                        "\n~~~", assigneeName, status, result);
    }
}
