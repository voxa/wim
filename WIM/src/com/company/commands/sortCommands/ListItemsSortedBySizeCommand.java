package com.company.commands.sortCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.Story;
import com.company.engine.EngineConstants;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ListItemsSortedBySizeCommand implements Command {


    private final Engine engine;

    public ListItemsSortedBySizeCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getStories().size() == 0) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

        String result = engine.getStories()
                .values()
                .stream()
                .sorted(Comparator.comparing(Story::getStorySize))
                .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        return String.format("~~~ List of stories sorted by size" +
                "\n %s" +
                "\n~~~", result);
    }


}