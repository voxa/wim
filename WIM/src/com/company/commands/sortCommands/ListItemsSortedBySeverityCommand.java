package com.company.commands.sortCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.Bug;
import com.company.engine.EngineConstants;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ListItemsSortedBySeverityCommand implements Command {


        private final Engine engine;

    public ListItemsSortedBySeverityCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getBugs().size() == 0) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

            String result = engine.getBugs()
                .values()
                .stream()
                .sorted(Comparator.comparing(Bug::getBugSeverity))
                    .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        return String.format("~~~ List of bugs sorted by severity" +
                "\n %s" +
                "\n~~~", result);
    }

}