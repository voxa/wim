package com.company.commands.sortCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.EngineConstants;


import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ListItemsSortedByTitleCommand implements Command {


       private final Engine engine;

    public ListItemsSortedByTitleCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getWorkItems().size() == 0) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }


        String result = engine.getWorkItems()
                .values()
                .stream()
                .sorted(Comparator.comparing(WorkItem::getTitle))
                .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        return String.format("~~~ List of stories sorted by title" +
                "\n %s" +
                "\n~~~", result);
    }
}