package com.company.commands.sortCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.Feedback;
import com.company.engine.EngineConstants;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ListItemsSortedByRatingCommand implements Command {


    private final Engine engine;

    public ListItemsSortedByRatingCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getFeedbacks().size() == 0) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }

      String result = engine.getFeedbacks()
                .values()
                .stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .collect(Collectors.toList())
                .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        return String.format("~~~ List of feedbacks sorted by rating" +
                "\n %s" +
                "\n~~~", result);
    }

}

