package com.company.commands.sortCommands;

import com.company.contracts.engine.Command;
import com.company.contracts.engine.Engine;
import com.company.contracts.workItems.Task;
import com.company.engine.EngineConstants;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class ListItemsSortedByPriorityCommand implements Command {



    private final Engine engine;

    public ListItemsSortedByPriorityCommand( Engine engine) {

        this.engine = engine;
    }

    @Override
    public String execute(List<String> parameters) {


        if (engine.getTasks().size() == 0) {
            throw new IllegalArgumentException(EngineConstants.EmptyListMessage);
        }


        String result = engine.getTasks()
                .values()
                .stream()
                .sorted(Comparator.comparing(Task::getPriority))
                .collect(Collectors.toList())
                 .toString()
                .replace(",", "\n")
                .replace("[", "")
                .replace("]", "");


        return String.format("~~~ List of  task sorted by priority" +
                "\n %s" +
                "\n~~~", result);
    }

}

