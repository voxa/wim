Work Item Management (WIM) Console Application

Functional Requirements
Application should support multiple teams.

•	Each team has name, members and boards.
•	Each member has name, list of work items and activity history.
•	Each board has name, list of work items and activity history.
•	There are three types of work items: bug, story and feedback.
o	Each bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history.
o	Each story has ID, title, description, priority, size, status, assignee, comments and history.
o	Each feedback has ID, title, description, rating, status, comments and history.

Design the Class Hierarchy
Your task is to design an object-oriented class hierarchy to model the Work Item Management System, using the best practices for object-oriented design (OOD) and object-oriented programming (OOP): 
•	Use data encapsulation
•	Proper use inheritance and polymorphism
•	Proper use interfaces and abstract classes
•	Proper use static/final members
•	Proper use enums
•	Follow the principles of strong cohesion and loose coupling
•	Avoid duplicated code though abstraction, inheritance, and polymorphism and encapsulate correctly all fields

Validation
•	Member
o	Name should be unique in the application
o	Name is a string between 5 and 15 symbols.

•	Board
o	Name should be unique in the team.
o	Name is a string between 5 and 10 symbols.


•	Bug
o	Title is a string between 10 and 50 symbols.
o	Description is a string between 10 and 500 symbols.
o	Steps to reproduce is a list of strings.
o	Priority is one of the following: High, Medium, Low
o	Severity is one of the following: Critical, Major, Minor
o	Status is one of the following: Active, Fixed
o	Assignee is a member from the team.
o	Comments is a list of comments (string messages with author).
o	History is a list of all changes (string messages) that were done to the bug.


•	Story
o	Title is a string between 10 and 50 symbols.
o	Description is a string between 10 and 500 symbols.
o	Priority is one of the following: High, Medium, Low
o	Size is one of the following: Large, Medium, Small
o	Status is one of the following: NotDone, InProgress, Done
o	Assignee is a member from the team.
o	Comments is a list of comments (string messages with author).
o	History is a list of all changes (string messages) that were done to the story.


•	Feedback
o	Title is a string between 10 and 50 symbols.
o	Description is a string between 10 and 500 symbols.
o	Rating is an integer.
o	Status is one of the following: New, Unscheduled, Scheduled, Done
o	Comments is a list of comments (string messages with author).
o	History is a list of all changes (string messages) that were done to the feedback.


•	Additional Notes:
o	IDs of work items should be unique in the application i.e. if we have a bug with ID X then we can't have Story of Feedback with ID X.



Printing
•	Use Streaming API
•	Implement proper user input validation and display meaningful user messages
•	Implement proper exception handling
•	No other validations specified



Testing

•	Cover functionality with unit tests (80% code coverage)
