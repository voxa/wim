package tests.listCommands;

import com.company.commands.listCommands.ListAllPeopleCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.teamParts.BoardImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAllTeamBoardsTests {
    private Factory factory;
    private Engine engine;
    private ListAllPeopleCommand command;
    private List<String> parameters;

    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new ListAllPeopleCommand(engine);
        parameters = new ArrayList<>();
    }

    @Test()
    public void listAllBoards() {
        Team team = new TeamImpl( "Alpha");

        BoardImpl board1 = new BoardImpl("Board1");
        BoardImpl board2 = new BoardImpl("Board2");
        team.getBoards().put(board1.getName() ,board1);
        team.getBoards().put(board2.getName() ,board2);

        List<String> parameters = Arrays.asList("Alpha","Board1","Board2");
        String result = command.execute(parameters);

        String expected = "~~~ List of  team \"Alpha\" boards\n  " +
                "Board1\n" +
                "Board2\n" +
                "~~~";
        Assert.assertEquals(expected,result);
    }


}