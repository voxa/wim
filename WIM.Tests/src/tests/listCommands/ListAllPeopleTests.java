package tests.listCommands;

import com.company.commands.listCommands.ListAllPeopleCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.teamParts.MemberImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAllPeopleTests {
    private Factory factory;
    private Engine engine;
    private ListAllPeopleCommand command;
    private List<String> parameters;

    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new ListAllPeopleCommand(engine);
        parameters = new ArrayList<>();
    }

    @Test()
    public void listAllPeople() {
        MemberImpl member1 = new MemberImpl("pesho");
        MemberImpl member2 = new MemberImpl("gosho");

        engine.getMembers().put(member1.getName(), member1);
        engine.getMembers().put(member2.getName(), member2);
        List<String> parameters = Arrays.asList("pesho", "gosho");
        String result = command.execute(parameters);
        String expected = "~~~ List of all people  \n" +
                " pesho\n" +
                " gosho\n" +
                "~~~";
        Assert.assertEquals(expected,result);
    }


}
