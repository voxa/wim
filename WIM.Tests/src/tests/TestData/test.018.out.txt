*** Team "Alpha" created
*** Team "Beta22" created
*** Person "Martin" created
*** Person "Atanas" created
*** "Martin" added to team "Alpha"
*** "Atanas" added to team "Beta22"
*** Board "BoardAlpha" created in team Alpha
*** Board "BoardBeta" created in team Beta22
*** Bug Id1 TitleOfBug1 created in team "Alpha" and board "BoardAlpha"
*** Bug Id2 TitleOfBug2 created in team "Beta22" and board "BoardBeta"
*** Story Id3 TitleOfStory3 created in team "Alpha" and board "BoardAlpha"
*** Story Id4 TitleOfStory4 created in team "Beta22" and board "BoardBeta"
*** Feedback Id5 TitleOfFeedback5 created in team "Alpha" and board "BoardAlpha"
*** Feedback Id6 TitleOfFeedback6 created in team "Beta22" and board "BoardBeta"
~~~ List of workItems of type Bug
 Id1 Bug
       Title: TitleOfBug1   Description: This is description 11
       Priority: High   Assignee: No assignee   Severity: Major   Status: Active
       Steps to reproduce: No steps
       Comments: No comments
       History: No history
 Id2 Bug
       Title: TitleOfBug2   Description: This is description 11
       Priority: High   Assignee: No assignee   Severity: Major   Status: Active
       Steps to reproduce: No steps
       Comments: No comments
       History: No history
~~~
~~~ List of workItems of type Story
 Id3 Story
       Title: TitleOfStory3   Description: This is description 11
       Priority: Low   Assignee: No assignee   Size: Large   Status: Done
       Comments: No comments
       History: No history
 Id4 Story
       Title: TitleOfStory4   Description: This is description 22
       Priority: Low   Assignee: No assignee   Size: Small   Status: Done
       Comments: No comments
       History: No history
~~~
~~~ List of workItems of type Feedback
 Id5 Feedback
       Title: TitleOfFeedback5   Description: This is description 33
       Rating: 30   Status: Done
       Comments: No comments
       History: No history
 Id6 Feedback
       Title: TitleOfFeedback6   Description: This is description 33
       Rating: 30   Status: Done
       Comments: No comments
       History: No history
~~~