package tests.change;

import com.company.commands.changeCommands.ChangeAssigneeCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Bug;
import com.company.engine.EngineConstants;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.teamParts.MemberImpl;
import com.company.model.workItems.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ChangeAssigneeCommand_Should {


    private Factory factory;
    private Engine engine;
    private ChangeAssigneeCommand command;
    private List<String> parameters;
    private Member member;
    private Team team;
    private String verb;
    private Bug bug;


    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new ChangeAssigneeCommand(engine);
        parameters = new ArrayList<>();
        member = new MemberImpl("Borislav");
        team = new TeamImpl("otbor");
        bug = new BugImpl(1, "Bug Title1", "description na bug", "steps", Priority.HIGH, BugSeverity.MAJOR, Status.ACTIVE);

    }

    @Test
    public void addAssigneeWithValidParameters() {

        parameters = Arrays.asList(String.valueOf(1),"Borislav", "assign");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.WorkItemAssignedSuccessMessage, "Borislav","Bug",1);

        Assert.assertEquals(expected, result);

    }

    @Test
    public void errorWhenMoreParametersArePassed() {
        parameters = Arrays.asList(String.valueOf(1),"Borislav", "assign", "more parameters" );

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void errorWhenLessParametersArePassed() {
        parameters = Arrays.asList(String.valueOf(1),"assign");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }



    @Test
    public void errorWhenWorkItemDoesNotExist() {
        parameters = Arrays.asList(String.valueOf(2), "Borislav" , "assignee");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.WorkItemsNoExistsErrorMessage, 2);

        Assert.assertEquals(expected, result);
    }





    @Test
    public void errorWhenPersonDoesNotExist() {

        engine.getTeams().put(team.getTeamName(), team);

        parameters = Arrays.asList("Cvetan", team.getTeamName());

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.PersonNoExistsErrorMessage, "stoyan");

        Assert.assertEquals(expected, result);
    }


}