package tests.change;

import com.company.commands.changeCommands.ChangeCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Bug;
import com.company.contracts.workItems.Feedback;
import com.company.contracts.workItems.Story;
import com.company.engine.EngineConstants;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;
import com.company.model.teamParts.MemberImpl;
import com.company.model.workItems.BugImpl;
import com.company.model.workItems.FeedbackImpl;
import com.company.model.workItems.StoryImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Change_Should {


    private Factory factory;
    private Engine engine;
    private ChangeCommand command;
    private List<String> parameters;
    private Member member;
    private Team team;
    private Bug bug;
    private Story story;
    private Feedback feedback;


    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new ChangeCommand(engine);
        parameters = new ArrayList<>();
        member = new MemberImpl("Borislav");
        team = new TeamImpl("otbor");
        bug = new BugImpl(1, "Bug Title1", "description na bug", "steps", Priority.HIGH, BugSeverity.MAJOR, Status.ACTIVE);
        story = new StoryImpl(2, "Story Title1", "description na story", Priority.HIGH, StorySize.LARGE, Status.DONE);
        feedback = new FeedbackImpl(3, "Feedback Title1", "description na feedbacka", 10, Status.DONE);
    }


    @Test
    public void errorWhenWorkItemDoesNotExist() {
        parameters = Arrays.asList(String.valueOf(4), "Priority", "Low");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.WorkItemsNoExistsErrorMessage, 4);

        Assert.assertEquals(expected, result);
    }


    @Test
    public void changePriorityWithValidParameters() {

        parameters = Arrays.asList(String.valueOf(1), "Priority", "Low");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.ChangedTypeSuccessMessage, "Priority", "Bug", "Low");

        Assert.assertEquals(expected, result);

    }

    @Test
    public void errorWhenMoreParametersArePassed() {
        parameters = Arrays.asList(String.valueOf(1), "Priority", "Low", "more parameters");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void errorWhenLessParametersArePassed() {
        parameters = Arrays.asList(String.valueOf(1), "Priority");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }


//    @Test
//    public void errorWhenPersonDoesNotExist() {
//
//        engine.getTeams().put(team.getTeamName(), team);
//
//        parameters = Arrays.asList("Cvetan", team.getTeamName());
//
//        String result = command.execute(parameters);
//        String expected = String.format(EngineConstants.PersonNoExistsErrorMessage, "stoyan");
//
//        Assert.assertEquals(expected, result);
//    }


}