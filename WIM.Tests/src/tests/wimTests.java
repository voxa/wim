package tests;

import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class wimTests {
    // The tests are located in the Tests folder
    // Each test has two files - "in" and "out"
    // The "in" file is the input
    // The "out" file is the expected output of your program


   @Test
    public void test_001() throws Exception {
        executeIOTest("001");
    }

    @Test
    public void test_002() throws Exception {
        executeIOTest("002");
    }

    @Test
    public void test_003() throws Exception {
        executeIOTest("003");
    }

    @Test
    public void test_004() throws Exception {
        executeIOTest("004");
    }

    @Test
    public void test_005() throws Exception {
        executeIOTest("005");
    }

    @Test
    public void test_006() throws Exception {
        executeIOTest("006");
    }

    @Test
    public void test_007() throws Exception {
        executeIOTest("007");
    }

    @Test
    public void test_008() throws Exception {
        executeIOTest("008");
    }

    @Test
    public void test_009() throws Exception {
        executeIOTest("009");
    }

    @Test
    public void test_010() throws Exception {
        executeIOTest("010");
    }

    @Test
    public void test_011() throws Exception {
        executeIOTest("011");
    }

    @Test
    public void test_012() throws Exception {
        executeIOTest("012");
    }

    @Test
    public void test_013() throws Exception {
        executeIOTest("013");
    }

    @Test
    public void test_014() throws Exception {
        executeIOTest("014");
    }

    @Test
    public void test_015() throws Exception {
        executeIOTest("015");
    }

    @Test
    public void test_016() throws Exception {
        executeIOTest("016");
    }

    @Test
    public void test_017() throws Exception {
        executeIOTest("017");
    }

    @Test
    public void test_018() throws Exception {
        executeIOTest("018");
    }

    @Test
    public void test_019() throws Exception {
        executeIOTest("019");
    }

    @Test
    public void test_020() throws Exception {
        executeIOTest("020");
    }

    @Test
    public void test_021() throws Exception {
        executeIOTest("021");
    }

    @Test
    public void test_022() throws Exception {
        executeIOTest("022");
    }

    @Test
    public void test_023() throws Exception {
        executeIOTest("023");
    }

    @Test
    public void test_024() throws Exception {
        executeIOTest("024");
    }

    @Test
    public void test_025() throws Exception {
        executeIOTest("025");
    }

    @Test
    public void test_026() throws Exception {
        executeIOTest("026");
    }


    @Test
    public void test_027() throws Exception {
        executeIOTest("027");
    }

    @Test
    public void test_028() throws Exception {
        executeIOTest("028");
    }
/*
     java.nio.file.NoSuchFileException
  String filename="result.csv";
    Path pathToFile = Paths.get(filename);
    System.out.println(pathToFile.getAbsolutePath());

/*
    private void executeIOTest(String testNumber) throws Exception {
        String testInputFilePath = "./WIM.Tests/src/tests/TestData/test." + testNumber + ".in.txt";
        InputStream testInput = new FileInputStream(testInputFilePath);
        System.setIn(testInput);

        ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputByteStream, true);
        System.setOut(output);

        Factory factory = new FactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();
//C:\Users\ASUS\IdeaProjects\wim\WIM.Tests\src\tests\TestData
        String testOutputFilePath = "./WIM.Tests/src/tests/TestData/test." + testNumber + ".out.txt";
        byte[] testOutputData = Files.readAllBytes(Paths.get(testOutputFilePath));
        String expected = new String(testOutputData, StandardCharsets.UTF_8);
        String actual = new String(outputByteStream.toByteArray(), StandardCharsets.UTF_8);

        Assert.assertEquals(expected.trim(), actual.trim());
    }*/

    private void executeIOTest(String testNumber) throws Exception {
        String testInputFilePath = "./WIM.Tests/src/tests/TestData/test."+ testNumber + ".in.txt";
        InputStream testInput = new FileInputStream(testInputFilePath);
        System.setIn(testInput);

        ByteArrayOutputStream outputByteStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputByteStream, true);
        System.setOut(output);

        Factory factory = new FactoryImpl();
        Engine engine = new EngineImpl(factory);
        engine.start();

        String testOutputFilePath = "./WIM.Tests/src/tests/TestData/test." + testNumber + ".out.txt";
        FileInputStream fstream = new FileInputStream(testOutputFilePath);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuilder expected = new StringBuilder();
        String strLine;
        while ((strLine = br.readLine()) != null) {
            expected.append(strLine).append(System.lineSeparator());
        }
        in.close();


        fstream = new FileInputStream(testOutputFilePath);
        in = new DataInputStream(fstream);
        br = new BufferedReader(new InputStreamReader(in));
        StringBuilder actual = new StringBuilder();
        String strLine1;
        while ((strLine1 = br.readLine()) != null) {
            actual.append(strLine1).append(System.lineSeparator());
        }
        in.close();

        Assert.assertEquals(expected.toString().trim(), actual.toString().trim());
    }
}
