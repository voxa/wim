package tests.commands;

import com.company.commands.addCommands.AddMemberToTeamCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Member;
import com.company.engine.EngineConstants;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.teamParts.MemberImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AddMemberToTeamCommand_Should {


    private Factory factory;
    private Engine engine;
    private AddMemberToTeamCommand command;
    private List<String> parameters;
    private Member member;
    private Team team;


    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new AddMemberToTeamCommand(engine);
        parameters = new ArrayList<>();
        member = new MemberImpl("krasen");
        team = new TeamImpl("otbor");
    }

    @Test
    public void addPersonToTeamWithValidParameters() {

        engine.getTeams().put(team.getTeamName(), team);
        engine.getMembers().put(member.getName(), member);

        parameters = Arrays.asList(member.getName(), team.getTeamName());

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.MemberAddedToTeamSuccessMessage, member.getName(), team.getTeamName());

        Assert.assertEquals(expected, result);

    }

    @Test
    public void errorWhenMoreParametersArePassed() {
        parameters = Arrays.asList("Alpha", "Gosho", "Beta");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 2);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void errorWhenLessParametersArePassed() {
        parameters.add("abc");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 2);

        Assert.assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorWhenTeamDoesNotExist() {

        parameters = Arrays.asList("Krasen", "Team Ala bala");
      //  engine.getMembers().put(member.getName(),member);

        String result = command.execute(parameters);
    }

//    @Test
//    public void errorWhenPersonDoesNotExist() {
//
//        engine.getTeams().put(team.getTeamName(), team);
//
//        parameters = Arrays.asList("Cvetan", team.getTeamName());
//
//        String result = command.execute(parameters);
//        String expected = String.format(EngineConstants.PersonNoExistsErrorMessage, "stoyan");
//
//        Assert.assertEquals(expected, result);
//    }
//
//    @Test
//    public void errorWhenPersonAlreadyInTeam() {
//
//        engine.getTeams().put(team.getTeamName(), team);
//        engine.getMembers().put(member.getName(), member);
//        team.getMembers().put(member.getName(), member);
//
//        parameters = Arrays.asList(member.getName(), team.getTeamName());
//
//        String result = command.execute(parameters);
//        String expected = String.format(EngineConstants.MemberExistsErrorMessage, member.getName(), team.getTeamName());
//
//        Assert.assertEquals(expected, result);
//    }

}