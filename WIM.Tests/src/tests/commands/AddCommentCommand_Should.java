package tests.commands;

import com.company.commands.addCommands.AddCommentCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.contracts.teamParts.Board;
import com.company.contracts.teamParts.Member;
import com.company.contracts.workItems.Bug;
import com.company.engine.EngineConstants;
import com.company.engine.EngineImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.teamParts.BoardImpl;
import com.company.model.teamParts.MemberImpl;
import com.company.model.workItems.BugImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AddCommentCommand_Should {


    private Factory factory;
    private Engine engine;
    private AddCommentCommand command;
    private List<String> parameters;
    private Member member;
    private Team team;
    private Board board;
    private Bug bug;


    @Before
    public void beforeFunc() {

        engine = new EngineImpl();
        parameters = new ArrayList<>();
        command = new AddCommentCommand(engine);
        member = new MemberImpl("Krasen");
        team = new TeamImpl("Team Ala bala");
        board = new BoardImpl("Board1");
        bug = new BugImpl(1, "Bug Title1", "description na bug", "steps", Priority.HIGH, BugSeverity.MAJOR, Status.ACTIVE);

    }

    @Test
    public void errorWhenWorkItemDoNotExist() {

//        board.getWorkItem().add(bug);
//        team.getBoards().put(board.getName(), board);
//        team.getMembers().put(member.getName(), member);

        engine.getTeams().put(team.getTeamName(), team);
        engine.getBoards().put(board.getName(), board);
        engine.getMembers().put(member.getName(), member);
        engine.getWorkItems().put(bug.getId(), bug);
        System.out.println("hoho");
        parameters = Arrays.asList(String.valueOf(2), "Krasen", "tova e komentar");
      //  engine.getWorkItems().put(1, bug);
        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.WorkItemsNoExistsErrorMessage, 2);

        Assert.assertEquals(expected, result);
    }


    @Test
    public void addCommentWithValidParameters() {

        parameters = Arrays.asList(String.valueOf(1), "Krasen", "tova e komentar");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.CommentAddedSuccessMessage, member.getName(), "Bug", 1);

        Assert.assertEquals(expected, result);
    }


    @Test
    public void errorWhenLessParametersArePassed() {

        parameters = Arrays.asList("a", "b");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }

    @Test
    public void errorWhenMoreParametersArePassed() {

        parameters = Arrays.asList("1", "ThisIsTitle", "This is message", "One more message");

        String result = command.execute(parameters);
        String expected = String.format(EngineConstants.EnterParametersMessage, 3);

        Assert.assertEquals(expected, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorWhenIdIsNotIntParametersArePassed() {

        parameters = Arrays.asList("b", "ThisIsTitle", "This is message");

        String result = command.execute(parameters);

    }


}