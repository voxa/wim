package tests.create;

import com.company.contracts.workItems.Bug;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.FactoryImpl;
import com.company.model.enums.BugSeverity;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.workItems.BugImpl;
import org.junit.Assert;
import org.junit.Test;

public class CreateBugTests {

    @Test
    public void returnInstanceOfTypeWorkItem() {
        FactoryImpl factory = new FactoryImpl();
        Bug bug = factory.createBug(1, "Random Bug Title", "Random Bug Description", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE, "Random Team Name", " Random Board Name");
        Assert.assertTrue(bug instanceof WorkItem);
    }

    @Test()
    public void bugIsCorrect() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugTitleIsEmpty() {
        BugImpl bug = new BugImpl(1, "", "Random Bug Description", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsEmpty() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStepsnIsEmpty() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description", "", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugTitleIsSmallerThanMinValue() {
        BugImpl bug = new BugImpl(1, "Bug", "Random Bug Description", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsLargerThanValue() {

        StringBuilder description = new StringBuilder();
        for (int i = 0; i < 505; i++)
            description.append("a");

        BugImpl bug = new BugImpl(1, "titleOfBug", description.toString(), "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugTitleIsBiggerThanMaxValue() {
        BugImpl bug = new BugImpl(1, "Random Bug Title, Random Bug Title, Random Bug Title, Random Bug Title, Random Bug Title",
                "Random Bug Description", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugDescriptionIsSmallerThanMinValue() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Descr.", "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong1() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong2() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.SCHEDULED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong3() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.UNSCHEDULED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong4() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.NEW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong5() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.IN_PROGRESS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong6() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.MAJOR, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBugStatusIsWrong7() {
        BugImpl bug = new BugImpl(1, "Random Bug Title", "Random Bug Description",
                "steps", Priority.HIGH,
                BugSeverity.valueOf("sth"), Status.ACTIVE);
    }

}
