package tests.create;

import com.company.contracts.workItems.Feedback;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.FactoryImpl;
import com.company.model.enums.Status;
import com.company.model.workItems.FeedbackImpl;
import org.junit.Assert;
import org.junit.Test;

public class CreateFeedbackTests {

    @Test
    public void returnInstanceOfWorkItem() {
        FactoryImpl factory = new FactoryImpl();
        Feedback feedback = factory.createFeedback(1, "Random Feedback Title", "Random Feedback Description", 1,
                Status.SCHEDULED, "Random Team Name", "Random Board Name");
        Assert.assertTrue(feedback instanceof WorkItem);

    }

    @Test()
    public void feedbackIsCorrect() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "Random Feedback Description", 1,
                Status.SCHEDULED);
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackTitleIsSmallerThanMinValue() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Feedb.", "Random Feedback Description",
                1,
                Status.SCHEDULED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackTitleIsBiggerThanMaxValue() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback title, Random Feedback title, Random Feedback title, Random Feedback title",
                "Random Feedback Description",
                1,
                Status.UNSCHEDULED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackDescriptionIsSmallerThanMinValue() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "Descr.",
                1,
                Status.NEW);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackRatingIsNegative() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "Random Feedback Description",
                -10,
                Status.DONE);
    }
    // RATING IS INT !!!!!

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenFeedbackStatusIsWrong() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "Random Feedback Description",
                1,
                Status.ACTIVE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void titleIsEmpty() {
        FeedbackImpl feedback = new FeedbackImpl(1, "", "Random Feedback Description", 1,
                Status.SCHEDULED);

    }

    @Test(expected = IllegalArgumentException.class)
    public void descriptionIsEmpty() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "", 1,
                Status.SCHEDULED);

    }

    @Test(expected = IllegalArgumentException.class)
    public void statusIsEmpty() {
        FeedbackImpl feedback = new FeedbackImpl(1, "Random Feedback Title", "Random Feedback Description", 1,
                Status.valueOf(""));

    }



}
