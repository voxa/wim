package tests.create;

import com.company.contracts.workItems.Comment;
import com.company.engine.FactoryImpl;
import com.company.model.workItems.CommentImpl;
import org.junit.Test;

public class CreateCommentTests {
    @Test()
    public void commentIsCorrect() {
        Comment comment = new CommentImpl("Anton", "Just a comment");
    }
    @Test()
    public void commentAuthorIsEmpty() {
        Comment comment = new CommentImpl("", "Just a comment");
    }
    @Test()
    public void commentIsEmpty() {
        Comment comment = new CommentImpl("Author name", "");
    }

}
