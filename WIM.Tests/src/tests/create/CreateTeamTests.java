package tests.create;

import com.company.model.TeamImpl;
import org.junit.Test;

public class CreateTeamTests {

    @Test()
    public void teamNameIsCorrect() {
        TeamImpl team = new TeamImpl("Team Name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenTeamNameIsEmpty() {
        TeamImpl team = new TeamImpl("");
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenTeamAlreadyExists() {
        TeamImpl team = new TeamImpl("Team Name");
        TeamImpl team2 = new TeamImpl("Team Name");
        if (team.getTeamName().equals(team2.getTeamName())) {
            throw new IllegalArgumentException("Teams with identical names cannot exist!");
        }
    }
}

