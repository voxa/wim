package tests.create;

import com.company.contracts.teamParts.Board;
import com.company.engine.FactoryImpl;
import com.company.model.Team;
import com.company.model.TeamImpl;
import com.company.model.teamParts.BoardImpl;
import org.junit.Assert;
import org.junit.Test;

public class CreateBoardTests {


    @Test()
    public void boardIsCorrect() {
        BoardImpl board = new BoardImpl("Board");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsEmpty() {
        BoardImpl board = new BoardImpl("");
    }


    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsSmallerThanMinValue() {
        BoardImpl board = new BoardImpl("Name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsBiggerThanMaxValue() {
        BoardImpl board = new BoardImpl("Extremely Long Board Name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenBoardNameIsAlreadyCreated() {
        FactoryImpl factory = new FactoryImpl();
        Board board = factory.createBoard("Board Name", "Alpha");
        Board board2 = factory.createBoard("Board Name", "Alpha");
        if (board.getName().equals(board2.getName())) {
            throw new IllegalArgumentException("Cannot have two boards with identical names in the same team!");
        }


    }
}
