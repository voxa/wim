package tests.create;

import com.company.contracts.workItems.Story;
import com.company.contracts.workItems.WorkItem;
import com.company.engine.FactoryImpl;
import com.company.model.enums.Priority;
import com.company.model.enums.Status;
import com.company.model.enums.StorySize;
import com.company.model.workItems.StoryImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CreateStoryTests {

    @Test
    public void returnInstanceOfTypeWorkItem() {
        FactoryImpl factory = new FactoryImpl();
        Story story = factory.createStory(1, "Random Story name", "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE, "Random team name", "Random board name");
        Assert.assertTrue(story instanceof WorkItem);
    }


    @Test()
    public void storyIsCorrect() {
        StoryImpl story = new StoryImpl(1, "Random Story name", "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryTitleIsEmpty() {
        StoryImpl story = new StoryImpl(1, "", "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryDescriptionIsEmpty() {
        StoryImpl story = new StoryImpl(1, "Random Story name", "", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryTitleIsSmallerThanMinValue() {
        StoryImpl story = new StoryImpl(1, "Story", "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryTitleIsBiggerThanMinValue() {
        StoryImpl story = new StoryImpl(1, "Random Title Story, Random Title Story, Random Title Story, Random Title Story",
                "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryDescriptionIsSmallerThanMinValue() {
        StoryImpl story = new StoryImpl(1, "Random Story Title", "Descr.", Priority.HIGH,
                StorySize.LARGE, Status.NOT_DONE);
    }
    @Test(expected = IllegalArgumentException.class)
    public void throwWhenStoryStatusIsWrong() {
        StoryImpl story = new StoryImpl(1, "Random Story Title", "Random story description", Priority.HIGH,
                StorySize.LARGE, Status.ACTIVE);
    }


}
