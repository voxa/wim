package tests.create;

import com.company.commands.createCommands.CreatePersonCommand;
import com.company.contracts.engine.Engine;
import com.company.contracts.engine.Factory;
import com.company.engine.EngineConstants;
import com.company.engine.EngineImpl;
import com.company.engine.FactoryImpl;
import com.company.model.teamParts.MemberImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreateMemberTests {

    private Factory factory;
    private Engine engine;
    private CreatePersonCommand command;
    private List<String> parameters;

    @Before
    public void beforeFunc() {
        factory = new FactoryImpl();
        engine = new EngineImpl(factory);
        command = new CreatePersonCommand(factory, engine);
        parameters = new ArrayList<>();
    }

    @Test()
    public void memberIsCorrect() {
        MemberImpl member = new MemberImpl("Member name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsEmpty() {
        MemberImpl member = new MemberImpl("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsSmallerThanMinValue() {
        MemberImpl member = new MemberImpl("Name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsBiggerThanMaxValue() {
        MemberImpl member = new MemberImpl("Extremely Long Member Name");
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwWhenMemberNameIsAlreadyCreated() {
        MemberImpl member1 = new MemberImpl("krasen");

        engine.getMembers().put(member1.getName(), member1);
        List<String> parameters = Arrays.asList("krasen");

        command.execute(parameters);
    }

    @Test
    public void errorWhenParametersAreInvalid() {
        parameters = Arrays.asList("ime", "ime");

        String result = command.execute(parameters);
        String expected = EngineConstants.EnterPersonNameMessage;

        Assert.assertEquals(expected, result);
    }

}
